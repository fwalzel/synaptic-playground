var perceptron = null;
var index = 0;
var color_data = null;
var filtered_data = null;
var original_data = null;
var canvas = document.getElementById('c');
var context = canvas.getContext('2d');
var size = 125 * 125;
var trial = 0;
var px = null;
var i = 1;

var getData = function (imageObj) {
    context.drawImage(imageObj, 0, 0);

    var imageData = context.getImageData(0, 0, 125, 125);
    return imageData.data;
};

var train = function () {
    trial = 0;

    perceptron = new synaptic.Architect.Perceptron(20, 8, 3);
    color_data = getData(document.getElementById('input' + i));
    filtered_data = getData(document.getElementById('output' + i));
    original_data = getData(document.getElementById('original'));

    i++;

    if (i === 5) {
        i = 1;
    }

    iteration();
};

var iteration = function () {
    trial++;

    for (index = 0; index < size; index += 2) {
        px = pixel(color_data, 0, 0);
        px = px.concat(pixel(color_data, -1, -1));
        px = px.concat(pixel(color_data, 0, -1));
        px = px.concat(pixel(color_data, 1, -1));
        px = px.concat(pixel(color_data, -1, 0));
        px = px.concat(pixel(color_data, 1, 0));
        px = px.concat(pixel(color_data, -1, 1));
        px = px.concat(pixel(color_data, 0, 1));
        px = px.concat(pixel(color_data, 1, 1));
        perceptron.activate(px);
        perceptron.propagate(.12, pixel(filtered_data, 0, 0));
    }
    preview();
};

var pixel = function (data, ox, oy) {
    console.log(data);

    var y = index / 125 | 0;
    var x = index % 125;

    if (ox && (x + ox) > 0 && (x + ox) < 125)
        x += ox;
    if (oy && (y + oy) > 0 && (y + oy) < 125)
        y += oy;

    var red = data[((125 * y) + x) * 4];
    var green = data[((125 * y) + x) * 4 + 1];
    var blue = data[((125 * y) + x) * 4 + 2];

    return [red / 255, green / 255, blue / 255];
};

var preview = function () {
    document.getElementById('iterations').innerHTML = trial;

    var imageData = context.getImageData(0, 0, 125, 125);
    for (index = 0; index < size; index++) {
        var px = pixel(original_data, 0, 0);
        px = px.concat(pixel(original_data, -1, -1));
        px = px.concat(pixel(original_data, 0, -1));
        px = px.concat(pixel(original_data, 1, -1));
        px = px.concat(pixel(original_data, -1, 0));
        px = px.concat(pixel(original_data, 1, 0));
        px = px.concat(pixel(original_data, -1, 1));
        px = px.concat(pixel(original_data, 0, 1));
        px = px.concat(pixel(original_data, 1, 1));
        var rgb = perceptron.activate(px);
        imageData.data[index * 4] = (rgb[0] ) * 255;
        imageData.data[index * 4 + 1] = (rgb[1] ) * 255;
        imageData.data[index * 4 + 2] = (rgb[2] ) * 255;
    }
    context.putImageData(imageData, 0, 0);

    // setTimeout(iteration, 50);
};

$(function() {
    train();
});