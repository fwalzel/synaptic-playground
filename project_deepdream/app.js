$(document).ready(function(){

    "use strict";

    var myNetwork = new synaptic.Architect.Perceptron(25*25, 240, 25*25),
        trainer = new synaptic.Trainer(myNetwork),
        trainingSet = [],
        numImgs = 110,
        prefix = '',
        input,
        results;

    var master = new Imageworker('input', [25,25]);
    master.monocrome([25,25], '#33FF00');
    input = master.serialize();

    var worker = new Imageworker('output', [25,25]);

    for (var i=0; i<numImgs+1; i++) {

        var img = '<img src="images/chair_'+i+'.jpg" id="ipt-'+i+'" style="float:left"/>';
        $('body').append(img);
    }

    function makeData() {

        for (var i=0; i<numImgs+1; i++) {
            trainingSet.push(
                {
                    input: worker.serialize('ipt-' + i),
                    output: worker.serialize('ipt-' + i)
                }
            );
            $('#ipt-' + i).css('border', '1px solid red');
        }

        console.log(trainingSet);
    }

    setTimeout(makeData, 5000);


    function train() {

        console.log('start training');

        return trainer.trainAsync(
            trainingSet,
            {
                iterations: 10000,
                log: 100,
                cost: synaptic.Trainer.cost.CROSS_ENTROPY
            }).then(
            function(res) {

                console.log('done!', res);

                var theResult = myNetwork.activate( input );
                console.log( theResult );
                worker.unserialize(theResult);

            }
        );

    }

    setTimeout(train, 10000);

});
