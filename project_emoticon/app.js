/* ****************************** */
/* Facial Recognition             */
/* ****************************** */

'use strict';

var myNetwork = new synaptic.Architect.Perceptron(380, 16, 1),
    trainer = new synaptic.Trainer(myNetwork),
    trainingSet = [],
    results;

var canvas = $('#my-canvas')[0],
    ctx = canvas.getContext('2d'),
    ImgWidth = 19,
    ImgHeight = 20;

/**
 *
 * @param imageObj
 * @param context
 * @param width
 * @param height
 * @returns {Array}
 */
var serializeImg = function (imageObj, context, width, height) {

    var serialized = [];
    var imageData;

    context.drawImage(imageObj, 0, 0);

    for (var x=0; x<width; x++) {
        for (var y=0; y<height; y++) {

            imageData = context.getImageData(x, y, 1, 1);

            var r = imageData.data[0] & 0xFF;
            var g = imageData.data[1] & 0xFF;
            var b = imageData.data[2] & 0xFF;

            var rgbInteger = (r << 16) + (g << 8) + (b);

            serialized.push( rgbInteger / 100000000 );
        }
    }
    return serialized;
};

for (var i=0; i<12; i++) {

    trainingSet.push(
        {
            input : serializeImg( $('#input-'+i)[0], ctx, ImgWidth, ImgHeight ),
            output : $('#output-'+i).attr('data-output')
        }
    );
}

console.log(trainingSet);

results = trainer.trainAsync(
    trainingSet,
    {
        iterations: 10000,
        log: 200,
        cost: synaptic.Trainer.cost.CROSS_ENTROPY
    }).then(
    function(res) {

        console.log('done!', res);

        for (var i=0; i<6; i++) {
            var theResult = myNetwork.activate( serializeImg( $('#test-'+i)[0], ctx, ImgWidth, ImgHeight ) )[0].toFixed(3);
            console.log( theResult );
            $('#result-'+i).append( ((theResult > .5) ? '=> happy' : '=> not happy') + ' (' + theResult + ')' );
        }

    }
);
