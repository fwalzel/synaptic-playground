/* ****************************** */
/* Inversion                      */
/* ****************************** */

'use strict';

var myNetwork = new synaptic.Architect.Perceptron(2, 2, 2);
var trainer = new synaptic.Trainer(myNetwork);

var trainingSet = [
    {
        input: [0,0],
        output: [1,1]
    },
    {
        input: [1,0],
        output: [0,1]
    },
    {
        input: [0,1],
        output: [1,0]
    },
    {
        input: [1,1],
        output: [0,0]
    }
];

var results = trainer.trainAsync(
    trainingSet,
    {
        iterations: 20000,
        log: 1000
    }).then(
        function(results) {

            console.log('done!', results);

            var outputs = [];

            outputs.push({
                input: '0 0',
                output: [ myNetwork.activate([0,0])[0].toFixed(3), myNetwork.activate([0,0])[1].toFixed(3) ]
            });
            outputs.push({
                input: '0 1',
                output: [ myNetwork.activate([1,0])[0].toFixed(3), myNetwork.activate([1,0])[1].toFixed(3) ]
            });
            outputs.push({
                input: '1 0',
                output: [ myNetwork.activate([0,1])[0].toFixed(3), myNetwork.activate([0,1])[1].toFixed(3) ]
            });
            outputs.push({
                input: '1 1',
                output: [ myNetwork.activate([1,1])[0].toFixed(3), myNetwork.activate([1,1])[1].toFixed(3) ]
            });

            console.log(outputs);

        }
    );




